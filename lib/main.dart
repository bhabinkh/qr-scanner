import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';

void main() => runApp(MaterialApp(home: QRScanPage()));


class QRScanPage extends StatefulWidget {
  @override
  _QRScanPageState createState() => _QRScanPageState();
}

class _QRScanPageState extends State<QRScanPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: QRView(
              overlay: QrScannerOverlayShape(
                borderWidth: 0.1,
                borderLength: MediaQuery.of(context).size.width * 0.3,
                borderRadius: 10,
                borderColor: Colors.blueGrey,
                overlayColor: Colors.blueGrey.withOpacity(0.3),
                cutOutSize: 0.6 * MediaQuery.of(context).size.width,
              ),
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.black,
              ),
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.23),
              padding: EdgeInsets.symmetric(vertical: 13),
              child: Center(
                child: Text(
                  "QR scan"

                  ///to get string of qr code use "$qrText"
                  // localeBundle.fromJson('qr.scan'),
                  , style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),

          // for testing the widget
          // remove this container in build
          Positioned(
            top: 80,
            left: 0,
            right: 0,
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: 0.2 * MediaQuery.of(context).size.width),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 13),
              color: Colors.blueGrey,
              child: Text(
                "$qrText",
                
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
